﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGuessNumber
{
    // SOLID
    // S. Single Responsibility Principle(SRP)
    // Game settings moved to a separate class
    public class SettingsGame
    {
        public byte GuessesMax { get; init; } = 3;
        public int NumberMax { get; init; } = 10;
        public int NumberMin { get; init; } = 0;

        public SettingsGame() { }
    }
}
