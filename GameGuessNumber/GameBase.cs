﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGuessNumber
{
    public abstract class GameBase: IGameConsole
    {
        public GameBase()
        {
            Settings = new SettingsGame();
        }

        public GameBase(SettingsGame settings)
        {
            Settings = settings;
        }

        public byte Score { get; set; } = 0;
        public bool IsLuck { get; set; }
        public SettingsGame Settings { get; set; }

        public virtual void Start()
        {
            Console.WriteLine("Запуск игровой логики.");
        }

        public virtual void End()
        {
            Console.WriteLine("Конец игры.");
        }

        private void PrintMenu()
        {
            Console.WriteLine("");
            Console.WriteLine("Меню игры.");
        }

        protected virtual void RunLogic()
        {
            Console.WriteLine("Реализация игровой логики.");
        }

        protected int GetNumberFromPlayer()
        {
            while (true)
            {
                Console.Write("Введите число: ");
                string? input = Console.ReadLine();
                if (int.TryParse(input, out int number))
                {
                    return number;
                }
                {
                    Console.WriteLine("Не корректное число!");
                }
            }
        }

    }
}
