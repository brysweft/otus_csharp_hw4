﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGuessNumber
{
    public interface IMenuConsole
    {
        public void Start() { }
        public void End() { }
        private void PrintMenu() { }
    }
}
