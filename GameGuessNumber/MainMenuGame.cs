﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGuessNumber
{
    public class MainMenuGame: IMenuConsole
    {
        public void Start() {

            Console.WriteLine("***** Угадай число *****");
            Console.WriteLine();
            Console.WriteLine("Сыграйте в игру.");

            StartController();   

        }

        public void End()
        {
            Console.WriteLine("Спасибо за игру.");
        }

        private void StartController()
        {
            bool runing = true;
            while (runing)
            {
                PrintMenu();

                string answer = Console.ReadLine();

                switch (answer)
                {
                    case "1":
                        СollectionGames.PlayGameClassic();
                        break;
                    case "2":
                        СollectionGames.PlayGameVersionTwo();
                        break;
                    case "s":
                    case "ы":
                        ControllerGame.PrintStatistic();
                        break;
                    case "q":
                    case "й":
                        runing = false;
                        break;
                    default: 
                        Console.WriteLine("Неизвестная команда");
                        break;
                }

            }
        }

        private void PrintMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Меню:");
            Console.WriteLine("Классический режим: [1]");
            Console.WriteLine("Сложный режим: [2]");
            Console.WriteLine("Показать статистику: [s][ы]");
            Console.WriteLine("Выйти: [q][й]");
            Console.Write("Выберите действие: ");
        }

    }
}
