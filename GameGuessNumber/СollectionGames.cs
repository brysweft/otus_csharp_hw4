﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGuessNumber
{
    // SOLID
    // S. Single Responsibility Principle(SRP)
    // Class СollectionGames moved to a separate class from MainMenuGame class
    public class СollectionGames
    {
        public static void PlayGameClassic()
        {
            Console.WriteLine("Классическая игра");
            Console.WriteLine();

            GameClassic Game = new GameClassic();
            ControllerGame.PlayGame(Game);
        }
        public static void PlayGameVersionTwo()
        {
            Console.WriteLine("Сложный режим. Лотерея");
            Console.WriteLine();

            SettingsGame settingsGame = new SettingsGame { GuessesMax = 10, NumberMax = 100 };

            GameHardVersion Game = new GameHardVersion(settingsGame);

            ControllerGame.PlayGame(Game);
        }
    }
}
