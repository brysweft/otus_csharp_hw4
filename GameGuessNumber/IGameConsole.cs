﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGuessNumber
{
    // SOLID
    // I. Interface Segregation Principle (ISP)
    // Separated game mechanics and console methods
    public interface IGameConsole : IGame, IMenuConsole
    {
    }
}
