﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGuessNumber
{
    public class GameClassic: GameBase
    {

        public GameClassic() : base() {}

        public GameClassic(SettingsGame settings) : base(settings) {}

        public override void Start() {
            RunLogic();
        }

        public override void End() {

            if (IsLuck)
            {
                Console.WriteLine($"Поздравляю! Вам повезло."); ;
            }
            else {
                Console.WriteLine($"Вы не отгадали число (");   
            }
            Console.WriteLine($"Попробуйте еще раз!");
            Console.ReadLine();

        }

        protected override void RunLogic() {
            Random random = new Random();
            int number = random.Next(Settings.NumberMin, Settings.NumberMax);

            Console.WriteLine($"Игра загадала число от {Settings.NumberMin} до {Settings.NumberMax}.");
            Console.WriteLine($"После каждой попытки игра будет давать подсказку.");
            Console.WriteLine($"Попробуйте угадать его с {Settings.GuessesMax} попытки.");

            while (Score < Settings.GuessesMax)
            {
                Score++;
                int guess = GetNumberFromPlayer();
                if (number == guess)
                {
                    Console.WriteLine($"Верно, вы угадали!");
                    IsLuck = true;
                    break;
                }
                else
                {
                    Console.WriteLine($"Нет, не угадали. Это была попытка {Score}");
                    if (number > guess)
                    {
                        Console.WriteLine("Ваше число меньше загаданного числа");
                    }
                    else
                    {
                        Console.WriteLine("Ваше число больше загаданного числа");
                    }
                }
            }
            Console.WriteLine($"Загаданное число: {number}");
        }

    }

}
