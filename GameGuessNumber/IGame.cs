﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGuessNumber
{
    // SOLID
    // I. Interface Segregation Principle (ISP)
    // Separated game mechanics and console methods
    public interface IGame
    {
        public byte Score { get; set; }

        public bool IsLuck { get; set; }

        public SettingsGame Settings { get; set; }

        protected void RunLogic() { }

    }
}
