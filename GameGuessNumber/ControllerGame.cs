﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameGuessNumber
{
    // SOLID
    // S. Single Responsibility Principle(SRP)
    // Class ControllerGame moved to a separate class from MainMenuGame class
    public static class ControllerGame
    {

        public static List<IGameConsole> games = new List<IGameConsole>();

        // SOLID
        // O. Open/Closed Principle(OCP)
        // Functionality is implemented through an interface IGame
        // D. Dependency Inversion Principle(DIP)
        // The controller class is independent of the specific game type
        public static void PlayGame(IGameConsole game)
        {
            game.Start();
            game.End();

            games.Add(game);
        }

        // SOLID
        // L. Liskov Substitution Principle (LSP)
        // Implemented a unified logic for working with different types of games
        public static void PrintStatistic() {

            Console.WriteLine($"Сыгранно игр всего: {games.Count}");
            Console.WriteLine($"из них удачных: {games.Count(x => x.IsLuck)}");
            Console.WriteLine($"Самая быстрая игра. Использованно попыток: {games.Where(x => x.IsLuck).Min(x => x.Score)}");

        }

    }
}
